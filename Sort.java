
public class Sort
{
    public static void mergeSort(int list[])
    {
        if (list.length > 1){
	    
	    int[] firstHalf = new int[list.length / 2];
	    System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
	    mergeSort(firstHalf);

	    int secondHalfLength = list.length - list.length / 2;
	    int[] secondHalf = new int[secondHalfLength];
	    System.arraycopy(list, list.length / 2,
			     secondHalf, 0, secondHalfLength);
	    
	    mergeSort(secondHalf);
	    merge(firstHalf, secondHalf, list);
	}
    }
    
    public static void merge(int[] list1, int[] list2, int[] destination)
    {
        int cl1, cl2, cdest;
        cl1 = cl2 = cdest = 0;

        while (cl1 < list1.length && cl2 < list2.length){
	    if (list1[cl1] < list2[cl2])
		destination[cdest++] = list1[cl1++];
	    else
		destination[cdest++] = list2[cl2++];
	}

        while (cl1 < list1.length)
            destination[cdest++] = list1[cl1++];

        while (cl2 < list2.length)
            destination[cdest++] = list2[cl2++];
    }
}
